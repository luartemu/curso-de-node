// Requireds
const fs = require('fs');

let data = '';


let listar = (base, limite = 10) => {

    return new Promise((resolve, reject) => {
        let data = '';
        if (!Number(base) || !Number(limite)) {
            reject('Los valores deben de ser numeros');
            return;
        }
        for (let i = 1; i <= limite; i++) {
            data += `${base} * ${i} = ${base*i }\n`;
        }
        resolve(data);


    });

};

let crearArchivo = (base, limite = 10) => {
    return new Promise((resolve, reject) => {
        if (!Number(base)) {

            reject(`No es un numero`);
            return;
        }
        for (let i = 1; i <= limite; i++) {
            data += `${base}* ${i} =  ${base * i}  \n`;
        }

        fs.writeFile(`tablas/tabla-${base}-al-${limite}.txt`, data, (err) => {
            if (err)
                reject(err);
            else
                resolve(`tablas/tabla-${base}-al-${limite}.txt`);
        });

    });
}

module.exports = {
    crearArchivo,
    listar
}