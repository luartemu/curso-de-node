// Requireds
const { argv } = require('./config/yargs')
const { crearArchivo, listar } = require('./multiplicar/multiplicar.js'); // los corchetes es gracias a la destructuracion de archivos 

// console.log(argv);

let comando = argv._[0];

switch (comando) {
    case 'listar':
        listar(argv.base, argv.limite)
            .then(data => { console.log(data) })
            .catch(err => console.log(err));
        break;
    case 'crear':
        crearArchivo(argv.base, argv.limite)
            .then(archivo => { console.log(`El archivo creado: ${archivo}`) })
            .catch(err => console.log(err));

        break;

    default:
        console.log('Comando no reconocido');
}

// console.log(comando);